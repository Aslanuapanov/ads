$('.check input:checkbox').click(function() {
  $('.check input:checkbox').not(this).prop('checked', false);
});

let gruz = document.querySelector('.gruz')
let convert = document.querySelector('.convert')
let pos_number_itog = document.querySelector('.pos_number_itog')
let cost_kg = document.querySelector('.cost_kg')

gruz.addEventListener('click', () => {
  let pos_number = pos_number_itog.querySelector('.pos_number')
  pos_number.innerText = '4'
  cost_kg.style.display = 'block'
})
convert.addEventListener('click', () => {
  let pos_number = pos_number_itog.querySelector('.pos_number')
  pos_number.innerText = '3'
  cost_kg.style.display = 'none'
})

let select1_block = document.querySelector('.select1')
let select2_block = document.querySelector('.select2')

select1_block.addEventListener('change', () => {
  select1_block.style.color = '#FC671A'
  select1_block.style.borderColor = '#FC671A'
  select1_block.style.background = "url('../assets/img/arrow_orange.png') center no-repeat"
})
select2_block.addEventListener('change', () => {
  select2_block.style.color = '#FC671A'
  select2_block.style.borderColor = '#FC671A' 
})
let cost_input = document.querySelector('.kg_input')
let cost_info = document.querySelector('.cost_info')
cost_input.addEventListener('input', () => {
  if(cost_input.value > 10){
    cost_info.style.display = 'inline-flex'
    pos_number_itog.style.display = 'none'
  }else{
    cost_info.style.display = 'none'
    pos_number_itog.style.display = 'block'
  }
})
