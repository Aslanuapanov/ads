import 'bootstrap'
import './scss/index.scss'
let link = document.querySelector('.uslugi_link')
let express = document.querySelector('.express')
let uslugi_menu = document.querySelector('.uslugi_menu')
let menu_child = document.querySelector('.menu_child')
let dop_uslugi = document.querySelector('.dop_uslugi')
let menu_child_items = document.querySelector('.menu_child_items')
let modal_block_print = document.querySelector('.modal_block_print')
uslugi_menu.style.display = 'none'
menu_child.style.display = 'none'
menu_child_items.style.display = 'none'


link.onmouseover = function() {
  uslugi_menu.style.display = 'block'  
};

uslugi_menu.onmouseover = function() {
  uslugi_menu.style.display = 'block'  
};

uslugi_menu.onmouseout = function() {
  uslugi_menu.style.display = 'none'
};

express.onmouseover = function() {
  menu_child.style.display = 'block'  
};

express.onmouseout = function() {
  menu_child.style.display = 'none'
};

menu_child.onmouseover = function(){
  menu_child.style.display = 'block'
}
menu_child.onmouseout = function(){
  menu_child.style.display = 'none'
}


dop_uslugi.onmouseover = function() {
  menu_child_items.style.display = 'block'  
};

dop_uslugi.onmouseout = function() {
  menu_child_items.style.display = 'none'
};

menu_child_items.onmouseover = function(){
  menu_child_items.style.display = 'block'
  menu_child.style.display = 'block'
  
}
menu_child_items.onmouseout = function(){
  menu_child_items.style.display = 'none'
  menu_child.style.display = 'none'
}

// Burget menu script
$('.burger-menu-btn').click(function(e){
  e.preventDefault();
  $(this).toggleClass('burger-menu-lines-active');
  $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
  $('body').toggleClass('body-overflow');
});

// Modal script
let btn_ads = document.querySelector('.btn_ads')
let modal_list_obzor = document.querySelector('.modal_list_obzor')
let plus = document.querySelector('.plus')
let minus = document.querySelector('.minus')
let print_modal = document.querySelector('.print_modal')
let modal_block = document.querySelector('.modal_block')

btn_ads.addEventListener('click', () => {
  $('#exampleModal').modal('show')
})

plus.addEventListener('click', () => {
  plus.style.display = 'none'
  minus.style.display = 'block'
  modal_list_obzor.style.display = 'flex'
})

minus.addEventListener('click', () => {
  plus.style.display = 'block'
  minus.style.display = 'none'
  modal_list_obzor.style.display = 'none' 
})

function printElement(elem) {
  var domClone = elem.cloneNode(true);
  var $printSection = document.getElementById("printSection");
  if (!$printSection) {
      var $printSection = document.createElement("div");
      $printSection.id = "printSection";
      document.body.appendChild($printSection);
  }
  $printSection.innerHTML = "";
  $printSection.appendChild(domClone);
}

print_modal.addEventListener('click', function () {
  printElement(modal_block_print);
  window.print();
}) 
