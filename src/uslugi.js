let acc = document.getElementsByClassName("accordion");

for (let i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    let plus_uslugi = this.querySelector('.plus_uslugi');
    let minus_uslugi = this.querySelector('.minus_uslugi');
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
      panel.classList.remove("border")
      plus_uslugi.style.display = 'block'
      minus_uslugi.style.display = 'none'
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
      panel.classList.add("border")
      plus_uslugi.style.display = 'none'
      minus_uslugi.style.display = 'block'
    } 
  });
}