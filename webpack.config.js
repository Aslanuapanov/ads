const path = require('path')
const webpack = require('webpack');
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const {resolve} = require('path')
const isProd = process.env.NODE_ENV === 'production'
const isDev = !isProd

const jsLoaders = () => {
  const loaders = [
    {
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-env']
      }
    }
  ]

  return loaders
}

const filename = ext => isDev ? `bundle.${ext}` : `bundle.[hash].${ext}`

module.exports = {
  context: path.resolve(__dirname, 'src'),
  mode: 'development',
  entry: ['@babel/polyfill', './index.js', './map.js', './uslugi.js', './cost.js'],
  output: {
    filename: filename('js'),
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    extensions: ['.js'],
    alias: {
      '@': path.resolve(__dirname, 'src'),
    }
  },
  devtool: isDev ? 'source-map' : false,
  devServer: {
    contentBase : ['index.html'], 
    watchContentBase : true, 
    port: 3000,
    hot: isDev,
  },
  
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Popper: ['popper.js', 'default']
    }),
    new HTMLWebpackPlugin({
      template: '../src/index.html',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new HTMLWebpackPlugin({
      template: '../src/map.html.ejs',
      filename: 'map.html',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new HTMLWebpackPlugin({
      template: '../src/about.html.ejs',
      filename: 'about.html',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new HTMLWebpackPlugin({
      template: '../src/information.html.ejs',
      filename: 'information.html',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new HTMLWebpackPlugin({
      template: '../src/uslugi.html.ejs',
      filename: 'uslugi.html',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new HTMLWebpackPlugin({
      template: '../src/costdelivery.html.ejs',
      filename: 'costdelivery.html',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new HTMLWebpackPlugin({
      template: '../src/certificate.html.ejs',
      filename: 'certificate.html',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new HTMLWebpackPlugin({
      template: '../src/spravka.html.ejs',
      filename: 'spravka.html',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new HTMLWebpackPlugin({
      template: '../src/uslovia.html.ejs',
      filename: 'uslovia.html',
      minify: false,
      inject: true,
      attrs: ['img:src', 'link:href']
    }),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'src/favicon.ico'),
          to: path.resolve(__dirname, 'dist')
        },
        { from: '../src/index.html', to: './index.html' },
        { from: '../src/map.html.ejs', to: './map.html' },
        { from: '../src/about.html.ejs', to: './about.html' }

      ],
    }),
    new MiniCssExtractPlugin({
      filename: filename('css')
    }),
  ],
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: isDev,
              reloadAll: true
            }
          },
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: jsLoaders(),
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
              esModule: false,
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-url-loader',
            options: {
              name: 'img/[name].[ext]',
              limit: false,
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: 'img/[name].[ext]',
              limit: false,
              esModule: false,
            }
          },
        ],
      },
      {
        test:/\.html$/,
        use: [
          'html-loader?minimize=false'
        ],
      },
    ]
  }
}
